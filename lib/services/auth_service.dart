import 'dart:convert';
import 'dart:io';

import 'package:app_vreal/components/util/Exceptions.dart';
import 'package:app_vreal/services/webClient.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {

  String url = webClient.url;
  http.Client client = webClient().client;

  static const String resource = "auth/";

  Future<bool> login({required String email, required password}) async {
    http.Response response = await client.post(
      Uri.parse('${url}login'),
      body: {'email': email, 'password': password},
    );

    if (response.statusCode != 200) {
      String content = json.decode(response.body);
      switch (content) {
        case "Cannot find user":
          throw UserNotFindException();
      }
      throw HttpException(response.body);
    }
    saveUserInfos(response.body);
    return true;
  }

  Future<bool> register({required String email, required String password}) async {
    http.Response response = await client.post(
      Uri.parse('${url}register'),
      body: {'email': email, 'password': password},
    );
    checkException(response);

    saveUserInfos(response.body);
    return true;
  }

  saveUserInfos(String body) async {
    Map<String, dynamic> map = json.decode(body);

    String token = map["accessToken"];
    String email = map["user"]["email"];
    int id = map["user"]["id"];
    String name = map["user"]["name"];

    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("accessToken", token);
    preferences.setString("email", email);
    preferences.setString("name", name);
    preferences.setInt("id", id);
  }
}
