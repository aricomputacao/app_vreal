import 'package:http/http.dart';
import 'package:http_interceptor/models/interceptor_contract.dart';
import 'package:logger/logger.dart';

class LoggingInterceptor implements InterceptorContract {
  Logger logger = Logger();

  @override
  Future<BaseRequest> interceptRequest({required BaseRequest request}) async {
    logger.t("Requisição para ${request.url}"
        "\nCabeçalhos: ${request.headers}"
        "\nCorpo: ${request.toString()}");

    return request;
  }

  @override
  Future<BaseResponse> interceptResponse(
      {required BaseResponse response}) async {
    if (response.statusCode ~/ 100 == 2) {
      logger.i("Requisição para ${response.request?.url}"
          "\nStatus da Resposta: ${response.statusCode}"
          "\nCabeçalhos: ${response.headers}"
          "\nCorpo: ${response is Response ? response.body : ""}");
    } else {
      logger.e("Requisição para ${response.request?.url}"
          "\nStatus da Resposta: ${response.statusCode}"
          "\nCabeçalhos: ${response.headers}"
          "\nCorpo: ${response is Response ? response.body : ""}");
    }

    return response;
  }

  @override
  Future<bool> shouldInterceptRequest() {
    // TODO: implement shouldInterceptRequest
    return Future.value(true);
  }

  @override
  Future<bool> shouldInterceptResponse() {
    // TODO: implement shouldInterceptResponse
    return Future.value(true);
  }
}
