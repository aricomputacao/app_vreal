import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/screens/group/group_screen.dart';
import 'package:app_vreal/screens/home/home.dart';
import 'package:app_vreal/screens/login_screen/login_screen.dart';
import 'package:app_vreal/themes/my_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // Get.locale =  Locale('en', 'US');
    Get.locale = const Locale('pt', 'BR');
    Logger logger = Logger();
    logger.i(Get.locale.toString());
    return GetMaterialApp(
      title: "Valor Real",
      locale: Get.locale,
      // locale: Get.deviceLocale,
      fallbackLocale: const Locale('pt', 'BR'),
      translations: AppTranslation(),
      themeMode: ThemeMode.light,
      theme: MyTheme,
      debugShowCheckedModeBanner: false,
      home: const Home(),
      initialRoute: "/login",
      routes: {
        "/home": (context) =>  const Home(),
        "/login": (context) => LoginScreen(),
        "/group": (context) =>  const GroupScreen(),
      },
    );
  }
}
