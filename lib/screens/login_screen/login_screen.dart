import 'dart:async';
import 'dart:io';

import 'package:app_vreal/components/util/Exceptions.dart';
import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/screens/commom/confirmation_dialog.dart';
import 'package:app_vreal/screens/commom/exception_dialog.dart';
import 'package:app_vreal/services/auth_service.dart';
import 'package:app_vreal/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordlController = TextEditingController();

  AuthService service = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(212, 224, 244, 1.0),
      body: Container(
        padding: const EdgeInsets.all(24),
        margin: const EdgeInsets.all(40),

        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey.shade300),

          gradient: const LinearGradient(colors: AppColors.loginGradient),
          borderRadius: BorderRadius.circular(24),
        ),
        child: Form(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.all(2.0),
                    child: Image(
                      height: 180,
                      width: 180,
                      image: AssetImage("assets/images/img-6.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  Text(Label.nome_sistema.name.tr,
                      style: const TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.bold,
                        color: Colors.blueGrey,
                      )),
                  const Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Divider(thickness: 2, color: Colors.blueAccent),
                  ),
                  const Text("Entre ou Registre-se",
                      style: TextStyle(color: Colors.blueAccent)),
                  TextFormField(
                    controller: _emailController,
                    decoration: const InputDecoration(
                      label: Text(
                        "E-mail",
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                  ),
                  TextFormField(
                    controller: _passwordlController,
                    decoration: InputDecoration(
                      label: Text(
                        Label.senha.name.tr,
                        style: const TextStyle(color: Colors.blueAccent),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    maxLength: 16,
                    obscureText: true,
                  ),
                  ElevatedButton(
                      style: AppColors.buttonStyle,
                      onPressed: () {
                        login(context);
                      },
                      child: const Text("Continuar")),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  login(BuildContext context) async {
    String email = _emailController.text;
    String password = _passwordlController.text;

    service.login(email: email, password: password).then(
      (resultLogin) {
        if (resultLogin) {
          Navigator.pushNamed(context, "/home");
        }
      },
    ).catchError(
      (error) {
        var innerError = error as HttpException;
        showExceptionDialog(context, content: innerError.message);
      },
      test: (error) => error is HttpException,
    ).catchError((error) {
      showConfirmationDialog(context,
              content:
                  "Deseja criar um novo usuário com o e-mail $email e a senha $password inserida ",
              affirmativeOption: "Criar")
          .then((value) {
        if (value != null && value) {
          service
              .register(email: email, password: password)
              .then((resultRegister) {
            if (resultRegister) {
              Navigator.pushNamed(context, "home");
            }
          });
        }
      });
    }, test: (error) => error is UserNotFindException).catchError((error) {
      showExceptionDialog(context,
          content:
              "O servidor demorou a responder, tente novamente mais tarde.");
    }, test: (error) => error is TimeoutException);
  }
}
