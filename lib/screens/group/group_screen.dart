import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/components/util/main_app_bar.dart';
import 'package:app_vreal/components/util/menu.dart';
import 'package:app_vreal/grupos.dart';
import 'package:app_vreal/models/type.dart';
import 'package:app_vreal/screens/group/widgets/group_add.dart';
import 'package:app_vreal/screens/group/widgets/group_list.dart';
import 'package:app_vreal/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/group.dart';

class GroupScreen extends StatefulWidget {
  const GroupScreen({super.key});

  @override
  State<GroupScreen> createState() => _GroupScreenState();
}

class _GroupScreenState extends State<GroupScreen> {
  final Logger logger = Logger();
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {

    List<Widget> pages = [GroupList(list: groups), GroupAdd(group: Group(id: 0,name: ""),isEditing: false, list: groups,)];


    return Scaffold(
      appBar: MainAppBar(
          title: '${Label.gerenciar.name.tr} ${Label.grupo.name.tr}'),
      drawer: Menu(),
      bottomNavigationBar: BottomNavigationBar(
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.search_outlined),
            label: Label.consultar.name.tr,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_box_outlined),
            label: Label.cadastrar.name.tr,
          ),
        ],
        selectedItemColor: AppColors.bottomNavigationBarIconColor,
        currentIndex: _currentPage,
        onTap: (index) {
          _currentPage = index;
          setState(() {});
        },
      ),
      body: pages.elementAt(_currentPage),
    );
  }
}
