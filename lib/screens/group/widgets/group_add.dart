import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/models/group.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GroupAdd extends StatefulWidget {
  final Group group;
  final bool isEditing;
  List<Group> list;

  GroupAdd(
      {super.key,
      required this.group,
      required this.isEditing,
      required this.list});

  @override
  State<GroupAdd> createState() => _GroupAddState();
}

class _GroupAddState extends State<GroupAdd> {
  @override
  Widget build(BuildContext context) {
    final TextEditingController _idController = TextEditingController();
    final TextEditingController _nameController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          Label.cadastrar.name.tr,
          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),

        ),
        backgroundColor: Colors.lightBlueAccent,
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0.1,
        shadowColor: Colors.lightBlue,
        actions: [
          IconButton(
              onPressed: () {
                Group group = Group(
                    id: int.parse(_idController.text),
                    name: _nameController.text);
                registerGroup(context, group);
              },
              icon: Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: Colors. green,
                    border: Border.all(color: Colors.white30),
                    borderRadius: BorderRadius.circular(24)),
                child: const Icon(
                  Icons.save_outlined,
                  color: Colors.white70,
                ),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                decoration: const InputDecoration(
                    labelText: 'Id',
                    // icon: Icon(Icons.account_box),
                    focusColor: Colors.white10,
                    fillColor: Colors.grey),
                controller: _idController,
                keyboardType: TextInputType.number,
                style: const TextStyle(fontSize: 10),
                // expands: true,
                // minLines: null,
                // maxLines: null,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: Label.nome.name.tr,
                    // icon: Icon(Icons.account_box),
                    focusColor: Colors.white10,
                    fillColor: Colors.grey),
                controller: _nameController,
                keyboardType: TextInputType.name,
                style: const TextStyle(fontSize: 10),
                // expands: true,
                // minLines: null,
                // maxLines: null,
              ),
            ],
          ),
        ),
      ),
    );
  }

  registerGroup(BuildContext context, Group group) {
    widget.list.add(group);
    setState(() {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            "Registro salvo com sucesso",
          ),
          backgroundColor: Colors.green,
          showCloseIcon: true,

        ),
      );
    });
  }
}
