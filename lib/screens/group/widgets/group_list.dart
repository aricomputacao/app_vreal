import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/models/group.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GroupList extends StatefulWidget {
  const GroupList({super.key, required this.list});

  final List<Group> list;

  @override
  State<GroupList> createState() => _GroupListState();
}

class _GroupListState extends State<GroupList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Label.consultar.name.tr,
          style:
              const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.lightBlueAccent,
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0.1,
        shadowColor: Colors.lightBlue,
      ),
      body: ListView.builder(
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          return Dismissible(
            key: UniqueKey(),
            direction: DismissDirection.endToStart,
            background: Container(
              color: Colors.red,
              alignment: Alignment.center,
              padding: const EdgeInsets.only(left: 20.0),
              child: const Icon(
                Icons.delete,
                size: 32,
                color: Colors.white,
              ),
            ),
            child: Container(
              color: index % 2 == 0 ? Colors.grey[100] : Colors.white,
              child: ListTile(
                leading: const Icon(Icons.production_quantity_limits),
                title: Text(widget.list[index].name),
                iconColor: Colors.blueAccent,
                trailing: IconButton(
                  color: Colors.blue,
                  tooltip: Label.editar.name.tr,
                  enableFeedback: true,
                  splashColor: Colors.lightGreen,
                  icon: Icon(Icons.mode_edit_outline_outlined),
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text(
                          "Registro salvo com sucesso",
                        ),
                        backgroundColor: Colors.lightBlueAccent,
                        showCloseIcon: true,

                      ),
                    );
                  },
                ),
              ),
            ),
            onDismissed: (direction) {
              setState(() {
                widget.list.removeAt(index);
              });
            },
          );
        },
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.blue,
      //   onPressed: () {
      //     // createType(context);
      //   },
      //   tooltip: 'Add Tipo',
      //   child: const Icon(Icons.add, color: Colors.white),
      // ),
    );
  }
}
