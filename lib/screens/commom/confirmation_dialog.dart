import 'package:flutter/material.dart';

Future<dynamic>  showConfirmationDialog(
  BuildContext context, {
  String title = "Atenção!",
  String content = "Está certo disso?",
  String affirmativeOption = "Confirmar",
  String negativeOption = "Cancelar",
}) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context,false);
            },
            style: TextButton.styleFrom(backgroundColor: Colors.red[400]),
            child: Text(negativeOption.toUpperCase(),
                style: const TextStyle(
                    color: Colors.white70, fontWeight: FontWeight.bold)),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context,true);
            },
            style: TextButton.styleFrom(backgroundColor: Colors.green[400]),
            child: Text(affirmativeOption.toUpperCase(),
                style: const TextStyle(
                    color: Colors.white70, fontWeight: FontWeight.bold)),
          ),
        ],
      );
    },
  );
}
