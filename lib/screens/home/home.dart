import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/components/util/locale_controller.dart';
import 'package:app_vreal/components/util/main_app_bar.dart';
import 'package:app_vreal/components/util/menu.dart';
import 'package:app_vreal/controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {

    final HomeController homeController = Get.put(HomeController());

    return  Scaffold(
      appBar: MainAppBar(title: Label.patrimonio.name.tr),
      drawer: Menu(),
      body: Text("teste"),


    );
  }
}
