import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class HomeController extends GetxController{

  @override
  void onInit(){
    super.onInit();
  }
  var count = 0.obs;
  increment() => count.value++;
}