import 'package:flutter/material.dart';

class Group{
  int id;
  String name;

  Group({
    required this.id,
    required this.name,
});
}