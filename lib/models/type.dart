import 'package:app_vreal/models/group.dart';

class Type {
  String id;
  String name;
  int start;
  int end;

  Type(
      {required this.id,
      required this.name,
      required this.start,
      required this.end});
}

