import 'package:flutter/material.dart';

class AppColors {
  static const MaterialColor primaryColor = Colors.green;
  static const List<Color> menuGradient = [
    Color.fromRGBO(223,242,242, 1.0),
    Color.fromRGBO(215,240,246, 1.0),
  ];

  static const List<Color> loginGradient = [
    // Color.fromRGBO(66, 133, 244, 1.0),
    // Color.fromRGBO(90, 148, 244, 1.0),
    // Color.fromRGBO(115, 163, 244, 1.0),
    // Color.fromRGBO(164, 194, 244, 1.0),
    // Color.fromRGBO(212, 224, 244, 1.0),
    Color.fromRGBO(255, 255, 255, 1.0),
    Color.fromRGBO(255, 255, 255, 1.0),
  ];


  static Color buttonForeground = Colors.white;
  static Color buttonBackground = const Color.fromRGBO(66, 133, 244, 1.0);
  static Color drawerFontColor = const Color(0xFF49454F);
  static Color textLabelFontColors = const Color(0xFF2979FF);
  static Color? bottomNavigationBarIconColor = Colors.grey[800];



  static ButtonStyle buttonStyle = ElevatedButton.styleFrom(
      elevation: 0,
      foregroundColor: buttonForeground,
      backgroundColor: buttonBackground);
}
