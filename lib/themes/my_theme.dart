import 'package:app_vreal/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData MyTheme = ThemeData(
  primaryColor: AppColors.primaryColor,
  primarySwatch: AppColors.primaryColor,
  brightness: Brightness.light,
  textTheme: GoogleFonts.arimoTextTheme(),
  useMaterial3: true
  // fontFamily: 'Raleway',
  // textTheme: const TextTheme(
  //   bodyMedium: TextStyle(
  //     fontSize: 16,
  //   ),
  //   bodyLarge: TextStyle(
  //     fontSize: 20,
  //     fontWeight: FontWeight.bold,
  //   ),
  //   titleMedium: TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
  //   titleLarge: TextStyle(fontSize: 28, fontWeight: FontWeight.normal),
  //
  // ),
);
