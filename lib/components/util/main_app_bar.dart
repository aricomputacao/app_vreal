import 'package:flutter/material.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  final String title;

  const MainAppBar({required this.title ,super.key});

  @override
  Widget build(BuildContext context) {
    // LocaleController().update;
    return AppBar(
      title: Text(
        title,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.blue,
      iconTheme: const IconThemeData(
        color: Colors.white,
      ),
      shadowColor: Colors.blueAccent,
      elevation: 3.0,
      actions: <Widget>[
        // IconButton(onPressed: (){}, icon:   Icon(Icons.safety_check)),
      ],
      centerTitle: true,

    );
  }
}
