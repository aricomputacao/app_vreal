import 'package:app_vreal/components/util/app_translation.dart';
import 'package:app_vreal/components/util/logout.dart';
import 'package:app_vreal/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Menu extends StatelessWidget {
  const Menu({super.key});


  Future<Map<String, String>> getUserData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString('name') ?? 'Nome não encontrado';
    String email = prefs.getString('email') ?? 'Email não encontrado';
    return {'name': name, 'email': email};
  }

  @override
  Widget build(BuildContext context) {

    return Drawer(
      backgroundColor: Colors.white,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          FutureBuilder<Map<String, String>>(
              future: getUserData(),
              builder: (BuildContext context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  // Você pode mostrar um placeholder ou um loader aqui
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  // Trate o erro como achar melhor
                  // throw Exception();
                  return Text('Erro ao carregar o nome');
                } else if (snapshot.hasData) {
                  return UserAccountsDrawerHeader(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: AppColors.menuGradient,
                      ),
                    ),
                    accountName: Text(
                      snapshot.data!['name']!,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    accountEmail: Text(snapshot.data!['email']!,
                        style: Theme.of(context).textTheme.titleSmall),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.lightBlue,
                      child: Text(
                        "a",
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                    ),
                  );
                } else {
                  return const DrawerHeader(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: AppColors.menuGradient,
                      ),
                    ),
                    child: Text('Nome não disponível'),
                  );
                }
              }),
          ListTile(
            leading: const Visibility(
              visible: true,
              child: Icon(
                Icons.home,
                size: 30,
                color: Colors.blue,
              ),
            ),
            onTap: () => {
              Navigator.pushNamed(context, '/'),
            },
            title: Text(
              Label.inicio.name.tr,
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
          ExpansionTile(
            leading: const Icon(
              Icons.account_balance_rounded,
              color: Colors.blue,
              size: 30.0,
            ),
            // collapsedBackgroundColor: Colors.lightBlueAccent,
            title: Text(
              Label.patrimonio.name.tr,
              style: Theme.of(context).textTheme.titleMedium,
            ),
            children: <Widget>[
              ListTile(
                leading: const Visibility(
                  visible: true,
                  child: Icon(
                    Icons.arrow_forward_sharp,
                    size: 15,
                  ),
                ),
                onTap: () => {
                  Navigator.pushNamed(context, '/group'),
                },
                title: Text(
                  Label.grupo.name.tr,
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ),
              ListTile(
                leading: const Visibility(
                  visible: true,
                  child: Icon(
                    Icons.arrow_forward_sharp,
                    size: 15,
                  ),
                ),
                onTap: () => {},
                title: Text(
                  Label.tipo.name.tr,
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ),
            ],
          ),
          ListTile(
            leading: const Visibility(
              visible: true,
              child: Icon(
                Icons.exit_to_app_outlined,
                size: 30,
                color: Colors.blue,
              ),
            ),
            onTap: () => {logout(context)},
            title: Text(
              Label.sair.name.tr,
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
        ],
      ),
    );
  }
}
