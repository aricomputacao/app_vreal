import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LocaleController extends GetxController {


  final List<Map> locales = [

    {'name': 'English', 'locale': Locale('en', 'US')},

    {'name': 'Español', 'locale': Locale('es', 'AR')},

    {'name': 'Português', 'locale': Locale('pt', 'BR')}

  ];



  Future<void> updateLocale(Locale locale) async {

    Future.delayed(Duration(milliseconds: 200), () {

      // Atualiza locale do app

      Get.updateLocale(locale);

    });

  }

}