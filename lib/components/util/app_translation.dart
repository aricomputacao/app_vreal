import 'package:get/get.dart';

class AppTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'pt_BR': {
          Label.regiao.name: Label.regiao.pt_BR,
          Label.nucleo.name: Label.nucleo.pt_BR,
          Label.socio.name: Label.socio.pt_BR,
          Label.gerenciar.name: Label.gerenciar.pt_BR,
          Label.nome_sistema.name: Label.nome_sistema.pt_BR,
          Label.patrimonio.name: Label.patrimonio.pt_BR,
          Label.grupo.name: Label.grupo.pt_BR,
          Label.tipo.name: Label.tipo.pt_BR,
          Label.sair.name: Label.sair.pt_BR,
          Label.senha.name: Label.senha.pt_BR,
          Label.inicio.name: Label.inicio.pt_BR,
          Label.consultar.name: Label.consultar.pt_BR,
          Label.cadastrar.name: Label.cadastrar.pt_BR,
          Label.nome.name: Label.nome.pt_BR,
          Label.editar.name: Label.editar.pt_BR,
        },
        'en_US': {
          Label.regiao.name: Label.regiao.en_US,
          Label.nucleo.name: Label.nucleo.en_US,
          Label.socio.name: Label.socio.en_US,
          Label.gerenciar.name: Label.gerenciar.en_US,
          Label.nome_sistema.name: Label.nome_sistema.en_US,
          Label.patrimonio.name: Label.patrimonio.en_US,
          Label.grupo.name: Label.grupo.en_US,
          Label.tipo.name: Label.tipo.en_US,
          Label.sair.name: Label.sair.en_US,
          Label.senha.name: Label.senha.en_US,
          Label.inicio.name: Label.inicio.en_US,
          Label.consultar.name: Label.consultar.en_US,
          Label.cadastrar.name: Label.cadastrar.en_US,
          Label.nome.name: Label.nome.en_US,
          Label.editar.name: Label.editar.en_US,
        },
      };
}

enum Label {
  regiao(pt_BR:'Região', en_US: 'Region' ),
  socio(pt_BR:'Sócio', en_US: 'Partner' ),
  nucleo(pt_BR:'Núcleo', en_US: 'Core' ),
  gerenciar(pt_BR:'Gerenciar', en_US: 'Management' ),
  entrar(pt_BR:'Entrar', en_US: 'Enter' ),
  sair(pt_BR:'Sair', en_US: 'Exit' ),
  senha(pt_BR:'Senha', en_US: 'Password' ),
  nome_sistema(pt_BR:'Valor Real', en_US: 'Real Value' ),
  patrimonio(pt_BR:'Patrimônio', en_US: 'Patrimony' ),
  grupo(pt_BR:'Grupo', en_US: 'Group' ),
  tipo(pt_BR:'Tipo', en_US: 'Type' ),
  inicio(pt_BR:'Início', en_US: 'Home' ),
  consultar(pt_BR:'Consultar', en_US: 'Consult' ),
  cadastrar(pt_BR:'Cadastrar', en_US: 'Register' ),
  nome(pt_BR:'Nome', en_US: 'Name' ),
  editar(pt_BR:'Editar', en_US: 'Editar' ),
  ;
  const Label({required this.pt_BR, required this.en_US});

  final String pt_BR;
  final String en_US;
}
