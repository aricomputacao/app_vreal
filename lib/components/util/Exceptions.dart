import 'dart:convert' show json;
import 'dart:io';

import 'package:http/http.dart';

class UserNotFindException implements Exception{}
class TokenNotValidException implements Exception{}

checkException(Response response){
  if (response.statusCode == 200 || response.statusCode == 201 ) {
    return;
  }else {
    if (json.decode(response.body) == "jwt expired") {
      throw TokenNotValidException();
    }
    throw HttpException(response.body);
  }
}